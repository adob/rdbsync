## Installation Requirements

### Operating System
In order to use rdbsync as a service, a generated DEB package is provieded for
Debian based Linux operating systems like Debian Stretch or Ubuntu 17.10.

On Windows the raw `.jar` file has to be executed manually as a service wrapper is not
provided.

### Environment

* Java(TM) SE Runtime Environment (Build 1.8.0_151 or higher)
* MySQL Community Server (Version 5.7.20 or higher)

## Set up on a Linux virtual machine

* Create an Ubuntu virtual machine using Oracle VirtualBox. Make sure to have a **Bridged Network Adapter**
to have a network access from outside.
* Proceed with steps below

## Set up on a native OS

### Linux (Debian/Ubuntu)

If you already have Java 8 installed and a MySQL database system not necessarily on the same machine, proceed with step 5.

1. Run a new terminal.
2. Type `su` and enter your root password to get appropriate rights.
3. Execute following commands to install Oracle Java 8:
  - `add-apt-repository ppa:webupd8team/java` to add a PPA for Oracle's version of Java
  - `apt-get update` to refresh packlists
  - `apt-get install oracle-java8-installer` to install Oracle's version of Java
  - `java -version` to check the installation

4. Execute following commands to set up MySQL server:
  - `apt-get install mysql-server` Install the MySQL-Server. Enter a new root password if asked.
  - `mysql -p` and enter password to reach the command line interface of MySQL.
  - `CREATE USER 'tester'@'%' IDENTIFIED BY 'q1w2e3r4';` to create a new user named _tester_ with password _q1w2e3r4_.
  - `GRANT ALL PRIVILEGES ON * . * TO 'tester'@'%';` to grant appropriate privileges.

5. Execute following commands to install git and development tools for dpkg
  - `apt-get install git`

6. Set up rdbsync
  - `git clone https://gitlab-as.informatik.uni-stuttgart.de/obraliar/rdbsync.git` to clone the project
  - `cd rdbsync`
  - `./gradlew build preparePackage` - Build the project and prepare the DEB package
  - `dpkg -b deb rdbsync-server_1.0_all.deb` - Build the DEB package
  - `dpkg -i rdbsync-server_1.0_all.deb` to install rdbsync-server. This should also start the service.
  - `systemctl status rdbsync` to check the service status.



From now on rdbsync should be ready to accept connections on port 5433. You can track the log file using `journalctl --follow -u rdbsync` (_Ctrl+C_ to abort view).

You should be able to connect **comeda** with this synchronization and database server. _rdbsync-server_ is indepandant of the location of the MySQL server. So it is possible to use a database system on another host which is specified by the client.

### Windows

You can use IDEs like [IntelliJ](https://www.jetbrains.com/idea/) or [Eclipse](http://www.eclipse.org/downloads/eclipse-packages/?osType=win32&release=undefined)
to import and run the project.

Otherwise follow next steps.

1. Install git including _git-bash_ from https://gitforwindows.org/
2. Open a _git-bash_ emulator
3. Execute:
  - `git clone https://gitlab-as.informatik.uni-stuttgart.de/obraliar/rdbsync.git` to clone the project
  - `cd rdbsync`
  - `./gradlew build`

The executable _.jar_ file will be generated in `build/libs` directory within the project.
