package de.unistuttgart.ipvs.rdbsync;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.unistuttgart.ipvs.rdbsync.automaton.State;
import de.unistuttgart.ipvs.rdbsync.automaton.SyncAutomaton;
import de.unistuttgart.ipvs.rdbsync.automaton.Transition;
import de.unistuttgart.ipvs.rdbsync.event.DataRecordAddedNotificationEvent;
import de.unistuttgart.ipvs.rdbsync.event.DataRecordDeletedNotificationEvent;
import de.unistuttgart.ipvs.rdbsync.event.DataRecordUpdatedNotificationEvent;
import de.unistuttgart.ipvs.rdbsync.event.FullSyncNotificationEvent;
import de.unistuttgart.ipvs.rdbsync.exception.FatalProtocolException;
import de.unistuttgart.ipvs.rdbsync.exception.NoSuchTransitionException;
import de.unistuttgart.ipvs.rdbsync.exception.ParseException;
import de.unistuttgart.ipvs.rdbsync.model.Constants;
import de.unistuttgart.ipvs.rdbsync.model.DataRecord;
import de.unistuttgart.ipvs.rdbsync.rdb.RDBConnectionProperties;
import de.unistuttgart.ipvs.rdbsync.rdb.RDBConnector;
import de.unistuttgart.ipvs.rdbsync.rdb.RDBSynchronizer;
import de.unistuttgart.ipvs.rdbsync.stream.Stream;
import de.unistuttgart.ipvs.rdbsync.util.Composer;
import de.unistuttgart.ipvs.rdbsync.util.Parser;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * The core class for the implemented synchronization protocol. It pools {@link SyncAutomaton} and
 * {@link RDBSynchronizer} to facilitate correct synchronization between the local and remote database.
 *
 * {@link SyncJob}s are received asynchronously by the <code>EventBus</code> to initiate synchronization steps.
 *
 * {@link Stream} is used for communication between this server and the connected client.
 */
public class SyncProcessor {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private SyncAutomaton syncAutomaton;
    private RDBSynchronizer rdbSynchronizer;
    private EventBus eventBus;
    private Queue<SyncJob> jobQueue;
    private String identifier;

    private Stream stream;
    private boolean enabled;
    private boolean fullSyncPerformed;

    SyncProcessor() {
        syncAutomaton = new SyncAutomaton();
        rdbSynchronizer = new RDBSynchronizer();
        eventBus = SyncServer.getDefaultEventBus();
        jobQueue = new ConcurrentLinkedQueue<>();
        identifier = UUID.randomUUID().toString();
        enabled = true;
        fullSyncPerformed = false;
    }

    /**
     * Periodically executed method by {@link SyncServerThread}. It analyses the current state of the
     * {@link SyncAutomaton} and performs dependant on the state synchronization operations
     * according to the sync protocol. It also sends, receives and applies {@link Transition}s to
     * switch between states.
     */
    public void cycle() throws IOException, FatalProtocolException, SQLException {
        State currentState = syncAutomaton.getCurrentState();

        try {
            if (currentState == State.START) {
                Transition transition = Parser.parseTransition(receive());

                if (transition == Transition.CLIENT_RDB_CONNECT_REQ) {
                    applyAndPostTransition(transition);
                }

            } else if (currentState == State.CONNECTING) {
                String connectionString = Parser.parseDataLine(receive());

                try {
                    RDBConnector connector = new RDBConnector();
                    RDBConnectionProperties properties = Parser.parseRDBConnectionProperties(connectionString);
                    connector.connect(properties);

                    rdbSynchronizer.prepare(connector);

                    LOGGER.log(Level.INFO, "Database connection to " + properties.getHost() + " established.");
                    applyAndPostTransition(Transition.SERVER_RDB_CONNECT_SUCCESS);

                } catch (SQLException | ClassNotFoundException e) {
                    LOGGER.log(Level.SEVERE, e.getMessage());
                    applyAndPostTransition(Transition.SERVER_RDB_CONNECT_FAIL);
                }

            } else if (currentState == State.CONNECTION_FAILED) {
                postData("Connection to RDB failed. Please fix your connection settings.");
                receiveAndApplyTransition();

            } else if (currentState == State.PRIMARY_PULL) {
                Set<Integer> serverSidSet = new HashSet<>(rdbSynchronizer.getLocalSIDVector());
                Set<Integer> clientSidSet = new HashSet<>(Parser.parseIntVector(receiveData()));

                serverSidSet.removeAll(clientSidSet);

                ArrayList<DataRecord> missingRecords = rdbSynchronizer.getRecords(serverSidSet);
                postDataArray(Composer.getComposedDataRecordArray(missingRecords));

                applyAndPostTransition(Transition.SERVER_PRIMARY_PULL_COMPLETE);

            } else if (currentState == State.PRIMARY_PUSH) {
                ArrayList<DataRecord> newDataRecords = Parser.getParsedDataRecordArray(receiveDataArray());
                HashMap<Integer, Integer> sidCidMap = new HashMap<>();

                for (DataRecord newDataRecord : newDataRecords) {
                    int sid = rdbSynchronizer.insertRecord(newDataRecord);
                    sidCidMap.put(sid, newDataRecord.getCid());
                }

                try {
                    postData(Composer.composeIntMap(sidCidMap));
                    receiveAndApplyTransition(); // triggers possibly the rollback

                } catch (IOException | FatalProtocolException e) {
                    // Rollback
                    rdbSynchronizer.deleteRecords(new ArrayList<>(sidCidMap.keySet()));
                    throw e;
                }

            } else if (currentState == State.PRIMARY_CLIENT_UPDATE) {
                Map<Integer, Integer> clientSidVersionMap = Parser.parseIntMap(receiveData());

                ArrayList<DataRecord> newerRecords = rdbSynchronizer.getNewerRecords(clientSidVersionMap);
                postDataArray(Composer.getComposedDataRecordArray(newerRecords));

                applyAndPostTransition(Transition.CLIENT_PRIMARY_UPDATE_COMPLETE);

            } else if (currentState == State.PRIMARY_SERVER_UPDATE) {
                ArrayList<DataRecord> dataRecords = Parser.getParsedDataRecordArray(receiveDataArray());
                ArrayList<Integer> updatedSidList = new ArrayList<>();
                ArrayList<DataRecord> rollbackDataRecords = new ArrayList<>();

                for (DataRecord dataRecord : dataRecords) {
                    DataRecord rollbackDataRecord = rdbSynchronizer.getRecord(dataRecord.getSid());

                    if (rdbSynchronizer.updateOlderRecord(dataRecord)) {
                        updatedSidList.add(dataRecord.getSid());
                        rollbackDataRecords.add(rollbackDataRecord);
                    }
                }

                try {
                    postData(Composer.composeIntVector(updatedSidList));

                } catch (IOException e) {
                    // Rollback
                    for (DataRecord dataRecord : rollbackDataRecords) {
                        rdbSynchronizer.updateRecord(dataRecord);
                    }
                }

                receiveAndApplyTransition();

            } else if (currentState == State.PRIMARY_CLIENT_DELETE) {
                Set<Integer> serverSidSet = new HashSet<>(rdbSynchronizer.getLocalSIDVector());
                Set<Integer> clientSidSet = new HashSet<>(Parser.parseIntVector(receiveData()));

                clientSidSet.removeAll(serverSidSet);

                ArrayList<Integer> recordsToDelete = new ArrayList<>(clientSidSet);

                postData(Composer.composeIntVector(recordsToDelete));

                receiveAndApplyTransition();

            } else if (currentState == State.PRIMARY_SERVER_DELETE) {
                Map<Integer, Integer> clientSidVersionMap = Parser.parseIntMap(receiveData());
                Map<Integer, Integer> serverSidVersionMap = rdbSynchronizer.getLocalSidVersionMap();

                // This set contains also SID which are not present on the server and have also to be deleted on
                // the client. This case occurs when the server fails to post this set to the client (see below).
                Set<Integer> sidToDeleteSet = new HashSet<>(clientSidVersionMap.keySet());
                sidToDeleteSet.removeAll(serverSidVersionMap.keySet());

                Set<Integer> deletionRejectedSidSet = new HashSet<>();

                for (Map.Entry<Integer, Integer> clientSidVersionMapEntry : clientSidVersionMap.entrySet()) {
                    for (Map.Entry<Integer, Integer> serverSidVersionMapEntry : serverSidVersionMap.entrySet()) {
                        if (clientSidVersionMapEntry.getKey().equals(serverSidVersionMapEntry.getKey())) {
                            // This deleteion is not forced as an other client may have increased the version of this DataRecord in the meantime.
                            // In this case the client which deletes the DataRecord will get a new record with higher version number.
                            if (rdbSynchronizer.safeDeleteRecord(clientSidVersionMapEntry.getKey(), clientSidVersionMapEntry.getValue())) {
                                sidToDeleteSet.add(clientSidVersionMapEntry.getKey());

                            } else {
                                deletionRejectedSidSet.add(clientSidVersionMapEntry.getKey());
                            }
                        }
                    }
                }

                postData(Composer.composeIntVector(new ArrayList<>(sidToDeleteSet)));

                ArrayList<DataRecord> deletionRejectedDataRecords = rdbSynchronizer.getRecords(deletionRejectedSidSet);
                ArrayList<String> composedDeletionRejectedDataRecords = Composer.getComposedDataRecordArray(deletionRejectedDataRecords);

                postDataArray(composedDeletionRejectedDataRecords);

                receiveAndApplyTransition();

            } else if (currentState == State.PRIMARY_CONFLICT_PULL) {
                Map<Integer, Integer> serverSidVersionMap = rdbSynchronizer.getLocalSidVersionMap();
                Map<Integer, Integer> clientSidVersionMap = Parser.parseIntMap(receiveData());
                ArrayList<Integer> missingSidList = new ArrayList<>();

                ArrayList<DataRecord> conflictingDataRecords = new ArrayList<>();

                for (Map.Entry<Integer, Integer> clientSidVersionMapEntry : clientSidVersionMap.entrySet()) {
                    boolean found = false;

                    for (Map.Entry<Integer, Integer> serverSidVersionMapEntry : serverSidVersionMap.entrySet()) {
                        // serverSidVersionMapEntry.getValue() + 1 because the version of modified DataRecords are
                        // increased on the client side.
                        if (serverSidVersionMapEntry.getKey().equals(clientSidVersionMapEntry.getKey())) {
                            found = true;

                            if (serverSidVersionMapEntry.getValue() + 1 > clientSidVersionMapEntry.getValue()) {
                                conflictingDataRecords.add(rdbSynchronizer.getRecord(serverSidVersionMapEntry.getKey()));
                            }
                        }
                    }

                    if (!found) {
                        missingSidList.add(clientSidVersionMapEntry.getKey());
                    }

                }

                postDataArray(Composer.getComposedDataRecordArray(conflictingDataRecords));

                postData(Composer.composeIntVector(missingSidList));

                // Trigger FULL_SYNC on other threads if not performed
                if (!fullSyncPerformed) {
                    eventBus.post(new FullSyncNotificationEvent(identifier));
                    fullSyncPerformed = true;
                }

                applyAndPostTransition(Transition.PRIMARY_CONFLICT_PULL_COMPLETE);

            } else if (currentState == State.STANDBY) {
                // ENTRY POINT
                receiveAndApplyTransition();


            } else if (currentState == State.ACTIVE) {
                SyncJob syncJob = jobQueue.peek();

                if (syncJob != null) {
                    switch (syncJob.getOperation()) {
                        case ADD:
                            applyAndPostTransition(Transition.DATA_ADDED_NOTIFICATION);
                            break;
                        case UPDATE:
                            applyAndPostTransition(Transition.DATA_UPDATED_NOTIFICATION);
                            break;
                        case DELETE:
                            applyAndPostTransition(Transition.DATA_DELETED_NOTIFICATION);
                            break;
                        case FULL_SYNC:
                            applyAndPostTransition(Transition.FULL_SYNC_NOTIFICATION);
                            jobQueue.poll();
                            break;
                    }

                } else {
                    idle();
                    applyAndPostTransition(Transition.SERVER_ACK);
                }

            } else if (currentState == State.IMMEDIATE_PUSH) {
                DataRecord dataRecord = Parser.parseDataRecord(receiveData());
                int sid = rdbSynchronizer.insertRecord(dataRecord);
                dataRecord.setSid(sid);

                try {
                    postData(Composer.composeInt(sid));
                    eventBus.post(new DataRecordAddedNotificationEvent(identifier, dataRecord));

                } catch (IOException e) {
                    // Rollback
                    rdbSynchronizer.deleteRecord(sid);
                    throw e;
                }

                receiveAndApplyTransition();

            } else if (currentState == State.IMMEDIATE_PULL) {
                SyncJob syncJob = jobQueue.poll();
                DataRecord dataRecord = syncJob.getDataRecord();

                postData(Composer.composeDataRecord(dataRecord));
                applyAndPostTransition(Transition.IMMEDIATE_PULL_COMPLETE);

            } else if (currentState == State.IMMEDIATE_UPDATE_PUSH) {
                DataRecord dataRecord = Parser.parseDataRecord(receiveData());
                DataRecord rollbackDataRecord = rdbSynchronizer.getRecord(dataRecord.getSid());

                boolean updated = rdbSynchronizer.updateOlderRecord(dataRecord);
                boolean rollbackPresent = (rollbackDataRecord != null);

                try {
                    postData(Composer.composeBoolean(updated));
                    postData(Composer.composeBoolean(rollbackPresent));
                    if (updated) {
                        eventBus.post(new DataRecordUpdatedNotificationEvent(identifier, dataRecord));

                    } else if (rollbackPresent) {
                        postData(Composer.composeDataRecord(rollbackDataRecord));
                    }
                } catch (IOException e) {
                    // Rollback in case that the boolean updated is not received correctly
                    if (rollbackDataRecord != null) {
                        rdbSynchronizer.updateRecord(rollbackDataRecord);
                    }
                    throw e;
                }

                applyAndPostTransition(Transition.IMMEDIATE_UPDATE_PUSH_COMPLETE);

            } else if (currentState == State.IMMEDIATE_UPDATE_PULL) {
                SyncJob syncJob = jobQueue.poll();
                DataRecord dataRecord = syncJob.getDataRecord();

                postData(Composer.composeDataRecord(dataRecord));
                applyAndPostTransition(Transition.IMMEDIATE_UPDATE_PULL_COMPLETE);

            } else if (currentState == State.IMMEDIATE_DELETE_PUSH) {
                boolean confirmation = Parser.parseBoolean(receiveData());

                if (confirmation) {
                    DataRecord recordToDelete = Parser.parseDataRecord(receiveData());
                    rdbSynchronizer.deleteRecord(recordToDelete.getSid());
                    eventBus.post(new DataRecordDeletedNotificationEvent(identifier, recordToDelete));
                }

                applyAndPostTransition(Transition.IMMEDIATE_DELETE_PUSH_COMPLETE);

            } else if (currentState == State.IMMEDIATE_DELETE_PULL) {
                SyncJob syncJob = jobQueue.poll();
                DataRecord dataRecordToDelete = syncJob.getDataRecord();

                postData(Composer.composeDataRecord(dataRecordToDelete));

                applyAndPostTransition(Transition.IMMEDIATE_DELETE_PULL_COMPLETE);

            } else if (currentState == State.ERROR_END) {
                enabled = false; // Disable processor due to error state
            }

        } catch (ParseException | NoSuchTransitionException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            applyTransition(Transition.SERVER_END);
        }

        if (syncAutomaton.getCurrentState() == State.END) {
            enabled = false;
        }
    }

    /**
     * Receives and applies a {@link Transition} for the local {@link SyncAutomaton}.
     */
    private void receiveAndApplyTransition() throws IOException, FatalProtocolException {
        try {
            Transition transition = Parser.parseTransition(receive());
            syncAutomaton.apply(transition);

        } catch (ParseException | NoSuchTransitionException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            throw new FatalProtocolException();
        }
    }

    /**
     * Applies the given {@link Transition} locally before it is sent to the client.
     * @param transition {@link Transition} which should be applied and sent
     */
    private void applyAndPostTransition(Transition transition) throws IOException {
        syncAutomaton.apply(transition);
        postTransition(transition);
    }

    /**
     * Applies the given {@link Transition} locally.
     * @param transition {@link Transition} which should be applied
     */
    private void applyTransition(Transition transition) {
        syncAutomaton.apply(transition);
    }

    /**
     * Receives raw data from the {@link Stream}
     * @return Raw data as string
     */
    private String receive() throws IOException {
        return stream.read();
    }

    /**
     * Receives data which has been prepared on the server side. E.g. Composed Maps, ArrayLists or
     * {@link DataRecord}s.
     *
     * @return Composed data from the client side
     */
    private String receiveData() throws IOException, FatalProtocolException {
        try {
            return Parser.parseDataLine(receive());

        } catch (ParseException e) {
            throw new FatalProtocolException();
        }
    }

    /**
     * Receives multiple objects which have been composed on the client side. E.g.
     * An ArrayList of {@link DataRecord}s.
     *
     * @return Composed data array from the client side
     */
    private ArrayList<String> receiveDataArray() throws IOException, FatalProtocolException {
        ArrayList<String> dataArray = new ArrayList<>();
        int size = Parser.parseInt(receiveData());

        for (int i = 0; i < size; i++) {
            dataArray.add(receiveData());
        }

        return dataArray;
    }

    /**
     * Receives a transition from the client.
     *
     * @return received {@link Transition}
     */
    private Transition receiveTransition() throws IOException, FatalProtocolException {
        try {
            return Parser.parseTransition(receive());

        } catch (ParseException | NoSuchTransitionException e) {
            throw new FatalProtocolException();
        }
    }

    /**
     * Posts raw data to the client.
     *
     * @param data Data to be posted
     */
    private void postData(String data) throws IOException {
        stream.write(Composer.composeDataLine(data));
    }

    /**
     * Posts an ArrayList of raw data to the client.
     *
     * @param dataArray Raw data to be posted
     */
    private void postDataArray(ArrayList<String> dataArray) throws IOException {
        postData(Composer.composeInt(dataArray.size()));

        for (String line : dataArray) {
            postData(line);
        }
    }

    /**
     * Posts a {@link Transition} to the client.
     *
     * @param transition {@link Transition} to be posted
     */
    private void postTransition(Transition transition) throws IOException {
        stream.write(Composer.composeTransition(transition));
    }

    /**
     * Listening method for {@link DataRecordAddedNotificationEvent}. Creates and enqueues a new {@link SyncJob} with
     * ADD operation. A callback is avoided by checking the identifier of the sender.
     */
    @Subscribe
    public void listen(DataRecordAddedNotificationEvent event) {
        if (!event.getIdentifier().equals(identifier)) {
            SyncJob syncJob = new SyncJob(event.getDataRecord(), SyncJob.Operation.ADD);
            jobQueue.offer(syncJob);
        }
    }

    /**
     * Listening method for {@link DataRecordUpdatedNotificationEvent}. Creates and enqueues a new {@link SyncJob} with
     * UPDATE operation.  A callback is avoided by checking the identifier of the sender.
     */
    @Subscribe
    public void listen(DataRecordUpdatedNotificationEvent event) {
        if (!event.getIdentifier().equals(identifier)) {
            SyncJob syncJob = new SyncJob(event.getDataRecord(), SyncJob.Operation.UPDATE);
            jobQueue.offer(syncJob);
        }
    }

    /**
     * Listening method for {@link DataRecordDeletedNotificationEvent}. Creates and enqueues a new {@link SyncJob} with
     * DELETE operation.  A callback is avoided by checking the identifier of the sender.
     */
    @Subscribe
    public void listen(DataRecordDeletedNotificationEvent event) {
        if (!event.getIdentifier().equals(identifier)) {
            SyncJob syncJob = new SyncJob(event.getDataRecord(), SyncJob.Operation.DELETE);
            jobQueue.offer(syncJob);
        }
    }

    /**
     * Listening method for {@link FullSyncNotificationEvent}. Creates and enqueues a new {@link SyncJob} with
     * FULL_SYNC operation.  A callback is avoided by checking the identifier of the sender.
     */
    @Subscribe
    public void listen(FullSyncNotificationEvent event) {
        if (!event.getIdentifier().equals(identifier)) {
            SyncJob syncJob = new SyncJob(null, SyncJob.Operation.FULL_SYNC);
            jobQueue.offer(syncJob);
        }
    }


    public void setStream(Stream stream) {
        this.stream = stream;
    }

    /**
     * Disables the {@link SyncProcessor} after working off the current state.
     */
    public void disable() {
        this.enabled = false;
    }

    /**
     * @return <code>true</code> if the {@link SyncProcessor} is still enabled, else <code>false</code>.
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Used to reduce data throughput to the client. This is used in ACTIVE state before
     * switching to the STANDBY state whereby the <code>jobQueue</code> is empty.
     */
    private void idle() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) { /* do nothing */}
    }

    /**
     * Closes all streams.
     */
    public void closeStreams() throws IOException {
        stream.close();
    }
}
