package de.unistuttgart.ipvs.rdbsync;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import de.unistuttgart.ipvs.rdbsync.event.ShutdownEvent;
import de.unistuttgart.ipvs.rdbsync.exception.FatalProtocolException;
import de.unistuttgart.ipvs.rdbsync.model.Constants;
import de.unistuttgart.ipvs.rdbsync.stream.Stream;
import de.unistuttgart.ipvs.rdbsync.stream.StreamReader;
import de.unistuttgart.ipvs.rdbsync.stream.StreamWriter;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * A background task which is assigned to the client. For each client a new {@link SyncServerThread} is created.
 * It organizes and clocks the {@link SyncProcessor}.
 */
public class SyncServerThread extends Thread {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private Socket socket;
    private SyncProcessor syncProcessor;
    private EventBus eventBus;

    SyncServerThread(Socket socket) {
        this.socket = socket;
        this.syncProcessor = new SyncProcessor();
        this.eventBus = SyncServer.getDefaultEventBus();
    }

    @Override
    public void run() {
        LOGGER.log(Level.INFO, "Sync session started. Client: " + socket.getInetAddress());
        eventBus.register(syncProcessor);
        eventBus.register(this);

        try {
            Stream stream = new Stream(new StreamReader(socket.getInputStream()), new StreamWriter(socket.getOutputStream()));
            syncProcessor.setStream(stream);

            while (!isInterrupted() && syncProcessor.isEnabled()) {
                syncProcessor.cycle();
            }

            LOGGER.log(Level.INFO, "Closing socket for " + socket.getInetAddress());
            syncProcessor.closeStreams();
            socket.close();

        } catch (SocketException e) {
            LOGGER.log(Level.SEVERE, "Connection lost: " + e.getMessage());

        } catch (SocketTimeoutException e) {
            LOGGER.log(Level.INFO, "Timeout for " + socket.getInetAddress());

        } catch (IOException e) {
            LOGGER.log(Level.INFO, "Connection lost: " + e.getMessage());

        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());

        } catch (FatalProtocolException e) {
            LOGGER.log(Level.SEVERE, "The sync protocol has been violated: " + e.getMessage());
        }

        LOGGER.log(Level.INFO, "Sync session terminated. Client: " + socket.getInetAddress());
        eventBus.unregister(syncProcessor);
        eventBus.unregister(this);
    }

    /**
     * Listens for a {@link ShutdownEvent} to terminate the {@link SyncProcessor} in a graceful way.
     */
    @Subscribe
    public void listen(ShutdownEvent event) {
        syncProcessor.disable();
    }
}
