package de.unistuttgart.ipvs.rdbsync.rdb;

import de.unistuttgart.ipvs.rdbsync.model.DataRecord;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * Functional interface between {@link DataRecord}, its metadata and the local database system.
 */
public class RDBSynchronizer {

    private Connection connection;
    private String table;


    /**
     * Initialize the table structure for the local database.
     */
    public void prepare(RDBConnector rdbConnector) throws SQLException {
        connection = rdbConnector.getCurrentConnection();
        table = rdbConnector.getProperties().getDatabase().toUpperCase();

        String database = rdbConnector.getProperties().getDatabase().toUpperCase();

        connection.createStatement().execute("CREATE DATABASE IF NOT EXISTS " + database);
        connection.setCatalog(database);

        String sqlQuery = "CREATE TABLE IF NOT EXISTS " + database + "(" +
                "SID INT NOT NULL AUTO_INCREMENT, " +
                "VERSION INT NOT NULL, " +
                "TIMESTAMP INT NOT NULL, " +
                "CONTENT TEXT NULL, " +
                "PRIMARY KEY (SID)" +
                ") ENGINE = InnoDB;";

        connection.createStatement().execute(sqlQuery);
    }

    /**
     * @return An ArrayList of <code>SID</code> non-zero values which belong to DataRecords
     */
    public ArrayList<Integer> getLocalSIDVector() throws SQLException {
        ArrayList<Integer> idVector = new ArrayList<>();

        String sqlQuery = "SELECT SID FROM " + table + ";";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sqlQuery);

        while (resultSet.next()) {
            idVector.add(resultSet.getInt("SID"));
        }

        return idVector;
    }

    /**
     * @return A mapping between <code>SID</code> and <code>VERSION</code> values of DataRecords
     */
    public Map<Integer, Integer> getLocalSidVersionMap() throws SQLException {
        Map<Integer, Integer> sidVersionMap = new HashMap<>();

        String sqlQuery = "SELECT SID, VERSION FROM " + table + ";";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sqlQuery);

        while (resultSet.next()) {
            sidVersionMap.put(resultSet.getInt("SID"), resultSet.getInt("VERSION"));
        }

        return sidVersionMap;
    }

    /**
     * @return An ArrayList of {@link DataRecord}s which are assigned by the given <code>idSet</code>
     */
    public ArrayList<DataRecord> getRecords(Set<Integer> idSet) throws SQLException {
        return getRecords(new ArrayList<>(idSet));
    }

    private ArrayList<DataRecord> getRecords(ArrayList<Integer> sidList) throws SQLException {
        ArrayList<DataRecord> dataRecords = new ArrayList<>();

        for (int i = 0; i < sidList.size(); i++) {
            String sqlQuery = "SELECT * FROM " + table + " WHERE SID = " + sidList.get(i) + ";";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            if (resultSet.next()) {
                dataRecords.add(
                    new DataRecord(resultSet.getInt("SID"), 0, resultSet.getInt("VERSION"),
                            resultSet.getInt("TIMESTAMP"), resultSet.getString("CONTENT"))
                );
            }
        }

        return dataRecords;
    }

    /**
     * @return A {@link DataRecord} which is assigned to the given <code>SID</code>, <code>null</code> if not present
     * @throws SQLException
     */
    public DataRecord getRecord(int sid) throws SQLException {
        String sqlQuery = "SELECT * FROM " + table + " WHERE SID = " + sid + ";";

        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(sqlQuery);

        if (resultSet.next()) {
            return new DataRecord(resultSet.getInt("SID"), 0, resultSet.getInt("VERSION"),
                    resultSet.getInt("TIMESTAMP"), resultSet.getString("CONTENT"));
        }

        return null;
    }

    /**
     * Inserts the given {@link DataRecord} into the local database.
     *
     * @param record {@link DataRecord} to insert
     * @return Generated <code>SID</code> (Server side ID) by the local database system
     */
    public int insertRecord(DataRecord record) throws SQLException {
        String sqlQuery = "INSERT INTO " + table + "(VERSION, TIMESTAMP, CONTENT) VALUES (?, ?, ?);";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setInt(1, record.getVersion());
        preparedStatement.setInt(2, record.getTimestamp());
        preparedStatement.setString(3, record.getContent());

        preparedStatement.executeUpdate();

        ResultSet resultSet = preparedStatement.getGeneratedKeys();

        if (resultSet.next()) {
            return resultSet.getInt(1);

        } else {
            throw new SQLException("Generated key could not be returned.");
        }
    }

    /**
     * Updates the given {@link DataRecord} by <code>SID</code> value.
     *
     * @param dataRecord {@link DataRecord} to be updated
     */
    public void updateRecord(DataRecord dataRecord) throws SQLException {
        String sqlQuery = "UPDATE " + table + " SET VERSION = ?, TIMESTAMP = ?, CONTENT = ? WHERE SID = ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, dataRecord.getVersion());
        preparedStatement.setInt(2, dataRecord.getTimestamp());
        preparedStatement.setString(3, dataRecord.getContent());
        preparedStatement.setInt(4, dataRecord.getSid());

        preparedStatement.executeUpdate();
    }

    /**
     * Updates the given {@link DataRecord} by <code>SID</code> if and only if it's version is greater by 1 than
     * the <code>VERSION</code> value of the row.
     *
     * @param dataRecord {@link DataRecord} to be updated
     * @return <code>true</code> if the update was successful, else <code>false</code>
     */
    public boolean updateOlderRecord(DataRecord dataRecord) throws SQLException {
        String sqlQuery ="UPDATE " + table + " SET VERSION = ?, TIMESTAMP = ?, CONTENT = ?" +
                " WHERE SID = ? AND VERSION + 1 = ?;";

        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setInt(1, dataRecord.getVersion());
        preparedStatement.setInt(2, dataRecord.getTimestamp());
        preparedStatement.setString(3, dataRecord.getContent());
        preparedStatement.setInt(4, dataRecord.getSid());
        preparedStatement.setInt(5, dataRecord.getVersion());

        return  preparedStatement.executeUpdate() == 1;
    }

    /**
     * Deletes all rows which are affected by the given List of <code>SID</code> values.
     *
     * @param sidList {@link ArrayList} of <code>SID</code> values
     */
    public void deleteRecords(ArrayList<Integer> sidList) throws SQLException {
        for (int i = 0; i < sidList.size(); i++) {
            int sid = sidList.get(i);
            connection.createStatement().executeUpdate("DELETE FROM " + table + " WHERE SID = " + sid + ";");
        }
    }

    /**
     * Deletes the specific row which is affected by the given <code>SID</code> value.
     *
     * @param sid <code>SID</code> value
     */
    public void deleteRecord(int sid) throws SQLException {
        connection.createStatement().executeUpdate("DELETE FROM " + table + " WHERE SID = " + sid + ";");
    }

    public boolean safeDeleteRecord(int sid, int version) throws SQLException {
        String preparedSqlQuery = "DELETE FROM " + table + " WHERE SID = ? AND VERSION = ?;";

        PreparedStatement statement = connection.prepareStatement(preparedSqlQuery);
        statement.setInt(1, sid);
        statement.setInt(2, version);

        return statement.executeUpdate() == 1;
    }

    /**
     * Compares all records with the given KeySet (<code>SID</code> values) against the given ValueSet
     * (<code>VERSION</code> values). Rows with higher version number will be retrieved.
     *
     * @param sidVersionMap Mapping between <code>SID</code> and <code>VERSION</code> values
     * @return Newer {@link DataRecord}s
     */
    public ArrayList<DataRecord> getNewerRecords(Map<Integer, Integer> sidVersionMap) throws SQLException {
        ArrayList<DataRecord> dataRecords = new ArrayList<>();

        for (Map.Entry<Integer, Integer> serverSidVersionEntry : sidVersionMap.entrySet()) {
            String sqlQuery = "SELECT * FROM " + table + " WHERE SID = " + serverSidVersionEntry.getKey() +
                    " AND VERSION > " + serverSidVersionEntry.getValue() + ";";

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(sqlQuery);

            if (resultSet.next()) {
                dataRecords.add(new DataRecord(resultSet.getInt("SID"), 0, resultSet.getInt("VERSION"),
                        resultSet.getInt("TIMESTAMP"), resultSet.getString("CONTENT")));
            }
        }

        return dataRecords;
    }
}
