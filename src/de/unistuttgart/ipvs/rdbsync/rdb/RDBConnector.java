package de.unistuttgart.ipvs.rdbsync.rdb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


/**
 * Essential methods to establish a connection to the relational database management system.
 */
public class RDBConnector {

    private RDBConnectionProperties properties;
    private Connection connection;


    /**
     * Loads the JDBC driver and establishes a {@link Connection} to the relational database management system.
     *
     * @param properties Connection properties
     */
    public void connect(RDBConnectionProperties properties) throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");

        String url = "jdbc:mysql://" + properties.getHost() + ":" + properties.getPort();

        this.properties = properties;
        this.connection = DriverManager.getConnection(url, properties.getUsername(), properties.getPassword());
    }

    public RDBConnectionProperties getProperties() {
        return properties;
    }

    public Connection getCurrentConnection() {
        return this.connection;
    }
}
