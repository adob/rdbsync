package de.unistuttgart.ipvs.rdbsync;

import com.google.common.eventbus.EventBus;
import de.unistuttgart.ipvs.rdbsync.event.ShutdownEvent;
import de.unistuttgart.ipvs.rdbsync.model.Constants;

import javax.net.ssl.SSLServerSocketFactory;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Contains the main entry point for this application. It listens for incoming connection requests and assigns/starts
 * for each successful connection a new {@link SyncServerThread}.
 * Furthermore it manages the shutdown of all running threads and provides a general EventBus.
 */
public class SyncServer extends Thread {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private static final int SYNCSERVER_PORT = 5433;
    private static EventBus eventBus;

    private ExecutorService executorService;
    private SSLServerSocketFactory serverSocketFactory;

    private ServerSocket serverSocket;

    private SyncServer() {
        executorService = Executors.newCachedThreadPool();
        setProperties();
        serverSocketFactory = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
    }

    /**
     * Main entry point. Starts a new SyncServer thread.
     */
    public static void main(String[] args) {
        SyncServer syncServer = new SyncServer();
        syncServer.start();
        // Add a shutdown hook for external exit signals.
        Runtime.getRuntime().addShutdownHook(new Thread(syncServer::shutdown));
    }

    @Override
    public void run() {
        try {
            serverSocket = serverSocketFactory.createServerSocket(SYNCSERVER_PORT);
            LOGGER.log(Level.INFO, "Started listening on port " + SYNCSERVER_PORT);

            while (!isInterrupted()) {
                Socket clientSocket = serverSocket.accept();

                LOGGER.log(Level.INFO, "Accepting secure connection from " + clientSocket.getInetAddress());

                clientSocket.setSoTimeout(10000);
                executorService.execute(new SyncServerThread(clientSocket));
            }

        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
    }

    /**
     * Stops all running {@link SyncServerThread}s friendly.
     */
    private void shutdown() {
        try {
            LOGGER.log(Level.INFO, "Shutting down the sync server...");

            // notify all SyncServerThreads
            getDefaultEventBus().post(new ShutdownEvent());

            // prevent new connections
            serverSocket.close();

            // wait until running sync sessions are finished
            executorService.awaitTermination(6000, TimeUnit.MILLISECONDS);

            // interrupt this thread
            interrupt();

        } catch (InterruptedException | IOException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
        }
    }

    /**
     * Sets the default keystore and its password.
     */
    private void setProperties() {
        System.setProperty("javax.net.ssl.keyStore", "/etc/rdbsync/server.keyStore");
        System.setProperty("javax.net.ssl.keyStorePassword", "q1w2e3r4");
    }

    public static EventBus getDefaultEventBus() {
        if (eventBus == null) {
            eventBus = new EventBus();
        }
        return eventBus;
    }
}
