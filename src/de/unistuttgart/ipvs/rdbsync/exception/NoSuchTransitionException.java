package de.unistuttgart.ipvs.rdbsync.exception;


/**
 * Thrown by {@link de.unistuttgart.ipvs.rdbsync.util.Parser} when the required transition is not
 * enumerated in {@link de.unistuttgart.ipvs.rdbsync.automaton.Transition}.
 */
public class NoSuchTransitionException extends Exception {

    public NoSuchTransitionException() {
        super("The given transition does not exist.");
    }
}
