package de.unistuttgart.ipvs.rdbsync.exception;


/**
 * Thrown by {@link de.unistuttgart.ipvs.rdbsync.util.Parser} when the data which should be parsed
 * is not formatted in appropriate way.
 */
public class ParseException extends RuntimeException {

    public ParseException(String data) {
        super("The given raw data '" + data + "' does not fulfill the requierments.");
    }
}
