package de.unistuttgart.ipvs.rdbsync.exception;


/**
 * Thrown when the synchronization protocol has been violated and therefore it could not be resumed.
 */
public class FatalProtocolException extends Exception {

    private static final long serialVersionUID = 1L;

    public FatalProtocolException() {
        super();
    }

    public FatalProtocolException(String message) {
        super(message);
    }
}
