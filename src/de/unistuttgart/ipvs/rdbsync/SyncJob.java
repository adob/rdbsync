package de.unistuttgart.ipvs.rdbsync;

import de.unistuttgart.ipvs.rdbsync.model.DataRecord;

/**
 * Used to be enqueued in a {@link java.util.concurrent.ConcurrentLinkedQueue} within
 * {@link SyncProcessor}. It contains the {@link DataRecord} which is affected by the set operation.
 */
public class SyncJob {

    public enum Operation { ADD, UPDATE, DELETE, FULL_SYNC }

    private DataRecord dataRecord;
    private Operation operation;

    SyncJob(DataRecord dataRecord, Operation operation) {
        this.dataRecord = dataRecord;
        this.operation = operation;
    }

    public DataRecord getDataRecord() {
        return dataRecord;
    }

    public Operation getOperation() {
        return operation;
    }
}
