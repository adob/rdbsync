package de.unistuttgart.ipvs.rdbsync.automaton;


/**
 * Contains all transitions which are essential for the automaton used by the synchronization
 * protocol. A transition has as pattern: <code>NAME(ORIGIN_STATE, TARGET_STATE)</code>.
 */
@SuppressWarnings("unused")
public enum Transition {

    CLIENT_RDB_CONNECT_REQ(State.START, State.CONNECTING),
    SERVER_RDB_CONNECT_FAIL(State.CONNECTING, State.CONNECTION_FAILED),
    CLIENT_RDB_CONNECT_ABORT(State.CONNECTION_FAILED, State.ERROR_END),

    SERVER_RDB_CONNECT_SUCCESS(State.CONNECTING, State.PRIMARY_PULL),

    SERVER_PRIMARY_PULL_COMPLETE(State.PRIMARY_PULL, State.PRIMARY_PUSH),

    CLIENT_PRIMARY_PUSH_COMPLETE(State.PRIMARY_PUSH, State.PRIMARY_CLIENT_UPDATE),

    CLIENT_PRIMARY_UPDATE_COMPLETE(State.PRIMARY_CLIENT_UPDATE, State.PRIMARY_SERVER_UPDATE),

    SERVER_PRIMARY_UPDATE_COMPLETE(State.PRIMARY_SERVER_UPDATE, State.PRIMARY_CLIENT_DELETE), /* received from client */

    CLIENT_PRIMARY_DELETE_COMPLETE(State.PRIMARY_CLIENT_DELETE, State.PRIMARY_SERVER_DELETE), /* received from client */

    PRIMARY_SERVER_DELETE_COMPLETE(State.PRIMARY_SERVER_DELETE, State.PRIMARY_CONFLICT_PULL), /* received from client */

    PRIMARY_CONFLICT_PULL_COMPLETE(State.PRIMARY_CONFLICT_PULL, State.STANDBY),

    DATA_ADDED(State.STANDBY, State.IMMEDIATE_PUSH),
    IMMEDIATE_PUSH_COMPLETE(State.IMMEDIATE_PUSH, State.STANDBY),

    DATA_ADDED_NOTIFICATION(State.ACTIVE, State.IMMEDIATE_PULL),
    IMMEDIATE_PULL_COMPLETE(State.IMMEDIATE_PULL, State.STANDBY),

    DATA_UPDATED(State.STANDBY, State.IMMEDIATE_UPDATE_PUSH),
    IMMEDIATE_UPDATE_PUSH_COMPLETE(State.IMMEDIATE_UPDATE_PUSH, State.STANDBY),

    DATA_UPDATED_NOTIFICATION(State.ACTIVE, State.IMMEDIATE_UPDATE_PULL),
    IMMEDIATE_UPDATE_PULL_COMPLETE(State.IMMEDIATE_UPDATE_PULL, State.STANDBY),

    DATA_DELETED(State.STANDBY, State.IMMEDIATE_DELETE_PUSH),
    IMMEDIATE_DELETE_PUSH_COMPLETE(State.IMMEDIATE_DELETE_PUSH, State.STANDBY),

    DATA_DELETED_NOTIFICATION(State.ACTIVE, State.IMMEDIATE_DELETE_PULL),
    IMMEDIATE_DELETE_PULL_COMPLETE(State.IMMEDIATE_DELETE_PULL, State.STANDBY),

    FULL_SYNC_NOTIFICATION(State.ACTIVE, State.PRIMARY_PULL),

    SERVER_END(State.STANDBY, State.END),

    SERVER_ACK(State.ACTIVE, State.STANDBY),
    CLIENT_ACK(State.STANDBY, State.ACTIVE);

    private State origin;
    private State target;

    Transition(State origin, State target) {
        this.origin = origin;
        this.target = target;
    }

    public State getOrigin() {
        return origin;
    }

    public State getTarget() {
        return target;
    }

}
