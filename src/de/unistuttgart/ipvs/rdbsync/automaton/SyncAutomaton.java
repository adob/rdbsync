package de.unistuttgart.ipvs.rdbsync.automaton;

import de.unistuttgart.ipvs.rdbsync.model.Constants;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * Simulates a final state machine whereby {@link State} is used to represent states and
 * {@link Transition} is used to realize transitions between them.
 *
 * The initial state of this automaton is <code>State.START</code>.
 */
public class SyncAutomaton {

    private static final Logger LOGGER = Logger.getLogger(Constants.LOGGER_ID);

    private State currentState;

    public SyncAutomaton() {
        currentState = State.START;
    }

    /**
     * Performs a transition between the current state and a state which is defined as a target
     * in {@link Transition}.
     *
     * @param transition Transition which should be applied on the state machine
     */
    public void apply(Transition transition) {
        List<Transition> transitions = new ArrayList<>(EnumSet.allOf(Transition.class));
        for (int i = 0; i < transitions.size(); i++) {
            if (transitions.get(i).getOrigin() == currentState &&
                    transitions.get(i).getTarget() == transition.getTarget()) {
                currentState = transitions.get(i).getTarget();
            }
        }
        //LOGGER.log(Level.FINE, "State: " + currentState);
        System.out.println("State: " + currentState);
    }

    public State getCurrentState() {
        return currentState;
    }
}
