package de.unistuttgart.ipvs.rdbsync.util;

import de.unistuttgart.ipvs.rdbsync.automaton.Transition;
import de.unistuttgart.ipvs.rdbsync.exception.NoSuchTransitionException;
import de.unistuttgart.ipvs.rdbsync.exception.ParseException;
import de.unistuttgart.ipvs.rdbsync.model.DataRecord;
import de.unistuttgart.ipvs.rdbsync.rdb.RDBConnectionProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


/**
 * A helper to decompose data which has been composed by {@link Composer} and received over
 * SSL sockets. {@link Composer} provides for each method in {@link Parser} a method with reverse
 * functionality.
 */
public class Parser {

    /**
     * @param connectionString Composed {@link RDBConnectionProperties}
     * @return Parsed {@link RDBConnectionProperties} object
     */
    public static RDBConnectionProperties parseRDBConnectionProperties(String connectionString) throws ParseException {
        String[] splittedConnectionString = connectionString.split(":", 5);

        if (splittedConnectionString.length != 5) {
            throw new ParseException(connectionString);
        }

        RDBConnectionProperties rdbConnectionProperties = new RDBConnectionProperties();

        rdbConnectionProperties.setHost(splittedConnectionString[0]);
        rdbConnectionProperties.setPort(splittedConnectionString[1]);
        rdbConnectionProperties.setDatabase(splittedConnectionString[2]);
        rdbConnectionProperties.setUsername(splittedConnectionString[3]);
        rdbConnectionProperties.setPassword(splittedConnectionString[4]);

        return rdbConnectionProperties;
    }

    /**
     * @param recordString Composed {@link DataRecord} object
     * @return Parsed {@link DataRecord}
     */
    public static DataRecord parseDataRecord(String recordString) throws ParseException {
        String[] splittedRecordString = recordString.split(":", 5);

        if (splittedRecordString.length != 5) {
            throw new ParseException(recordString);
        }

        int sid = Integer.parseInt(splittedRecordString[0]);
        int cid = Integer.parseInt(splittedRecordString[1]);
        int version = Integer.parseInt(splittedRecordString[2]);
        int timestamp = Integer.parseInt(splittedRecordString[3]);
        String content = splittedRecordString[4];

        return new DataRecord(sid, cid, version, timestamp, content);
    }

    /**
     * Parses the given raw data line.
     *
     * @param rawDataLine raw data
     * @return Decomposed value of the raw data
     */
    public static String parseDataLine(String rawDataLine) throws ParseException {
        String[] dataLine = rawDataLine.split("/", 2);

        if (dataLine.length != 2 || !dataLine[0].equals("d")) {
            throw new ParseException(rawDataLine);
        }
        return dataLine[1];
    }

    /**
     * @param rawTransition Raw Composed {@link Transition}
     * @return Decomposed {@link Transition}
     * @throws ParseException if the raw data cannot be parsed as an {@link Transition}
     * @throws NoSuchTransitionException if the decomposed {@link Transition} does not exist
     */
    public static Transition parseTransition(String rawTransition) throws ParseException, NoSuchTransitionException {
        String[] transition = rawTransition.split("/", 2);

        if (transition.length != 2 || !transition[0].equals("t")) {
            throw new ParseException(rawTransition);
        }

        try {
            return Transition.valueOf(Transition.class, transition[1].toUpperCase(Locale.ENGLISH));
        } catch (IllegalArgumentException e) {
            // IllegalArgumentException is thrown when the specific transition does not exist
            throw new NoSuchTransitionException();
        }
    }

    /**
     * @param vectorToParse Composed Integer vector
     * @return ArrayList of Integers
     */
    public static ArrayList<Integer> parseIntVector(String vectorToParse) {
        ArrayList<Integer> parsedVector = new ArrayList<>();
        String[] splittedVector = vectorToParse.split(":", -1);

        if (vectorToParse.isEmpty()) {
            return parsedVector;
        }

        for (int i = 0; i < splittedVector.length; i++) {
            parsedVector.add(Integer.parseInt(splittedVector[i]));
        }

        return parsedVector;
    }

    /**
     * @param mapToParse Raw Integer-Integer map to be parsed
     * @return Parsed Integer-Integer map
     */
    public static Map<Integer, Integer> parseIntMap(String mapToParse) throws ParseException {
        Map<Integer, Integer> parsedMap = new HashMap<>();
        String[] tuples = mapToParse.split(";", -1);

        if (mapToParse.isEmpty()) {
            return parsedMap;
        }

        for (int i = 0; i < tuples.length; i++) {
            String[] keyValue = tuples[i].split(":", -1);

            if (keyValue.length != 2) {
                throw new ParseException(mapToParse);
            }

            int key = Integer.parseInt(keyValue[0]);
            int value = Integer.parseInt(keyValue[1]);

            parsedMap.put(key, value);
        }

        return parsedMap;
    }

    /**
     * @param valueToParse Composed Integer value to be parsed
     * @return Parsed Integer
     */
    public static int parseInt(String valueToParse) throws ParseException {
        try {
            return Integer.parseInt(valueToParse);

        } catch (NumberFormatException e) {
            throw new ParseException(valueToParse);
        }
    }

    /**
     * @param valueToParse Composed Boolean value to be parsed
     * @return Parsed Boolean
     */
    public static boolean parseBoolean(String valueToParse) throws ParseException {
        switch (valueToParse) {
            case "1":
                return true;
            case "0":
                return false;
        }

        throw new ParseException(valueToParse);
    }

    /**
     * Converts an ArrayList of composed {@link DataRecord}s to a new ArrayList of parsed {@link DataRecord}s.
     *
     * @param dataRecordsToParse ArrayList which should be parsed
     * @return ArrayList of parsed {@link DataRecord}s
     */
    public static ArrayList<DataRecord> getParsedDataRecordArray(ArrayList<String> dataRecordsToParse) {
        ArrayList<DataRecord> parsedDataRecordArray = new ArrayList<>();
        for (String dataRecordToParse : dataRecordsToParse) {
            parsedDataRecordArray.add(parseDataRecord(dataRecordToParse));
        }
        return parsedDataRecordArray;
    }

}
