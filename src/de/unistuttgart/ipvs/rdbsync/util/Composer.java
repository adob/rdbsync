package de.unistuttgart.ipvs.rdbsync.util;

import de.unistuttgart.ipvs.rdbsync.automaton.Transition;
import de.unistuttgart.ipvs.rdbsync.model.DataRecord;

import java.util.ArrayList;
import java.util.Map;


/**
 * A helper to prepare data for sending over SSL sockets. {@link Parser} provides for each method
 * of {@link Composer} o method with reverse functionality.
 */
public class Composer {


    /**
     * @return Composed data line with appropriate delimiter
     */
    public static String composeDataLine(String line) {
        return "d/" + line;
    }

    /**
     * @param transition Transition to compose
     * @return Composed {@link Transition} with appropriate delimiter
     */
    public static String composeTransition(Transition transition) {
        return "t/" + transition.toString();
    }

    /**
     * Composes an ArrayList of Integers by separating them with ':' as delimiter.
     *
     * @param vectorToCompose List of Integers to compose
     * @return composed Integer vector
     */
    public static String composeIntVector(ArrayList<Integer> vectorToCompose) {
        StringBuilder vectorBuilder = new StringBuilder();

        for (int i = 0; i < vectorToCompose.size(); i++) {
            vectorBuilder.append(vectorToCompose.get(i));

            if (i < vectorToCompose.size() - 1 ) {
                vectorBuilder.append(":");
            }
        }

        return vectorBuilder.toString();
    }

    /**
     * @param record {@link DataRecord} to compose
     * @return Composed {@link DataRecord}
     */
    public static String composeDataRecord(DataRecord record) {
        return new StringBuilder()
                .append(record.getSid())
                .append(":")
                .append(record.getCid())
                .append(":")
                .append(record.getVersion())
                .append(":")
                .append(record.getTimestamp())
                .append(":")
                .append(record.getContent()).toString();
    }

    /**
     * Composes an Integer array by separating tuple values with ':' and tuples itself by a ';'.
     *
     * @param mapToCompose Map to be composed
     * @return Composed Integer-Integer map
     */
    public static String composeIntMap(Map<Integer, Integer> mapToCompose) {
        StringBuilder composedMap = new StringBuilder();

        int size = mapToCompose.entrySet().size();
        int i = 0;

        for (Map.Entry<Integer, Integer> sidCidEntry : mapToCompose.entrySet()) {
            int sid = sidCidEntry.getKey();
            int cid = sidCidEntry.getValue();

            composedMap.append(sid);
            composedMap.append(":");
            composedMap.append(cid);

            if (i < size - 1) {
                composedMap.append(";");
            }

            i++;
        }

        return composedMap.toString();
    }

    /**
     * @param valueToCompose Integer to be composed
     * @return composed Integer
     */
    public static String composeInt(int valueToCompose) {
        return String.valueOf(valueToCompose);
    }

    /**
     * Composes a Boolean value whereby <code>true</code> is projected to 1 and <code>false</code>
     * to 0.
     * @param valueToCompose Boolean to be composed
     * @return Composed Boolean
     */
    public static String composeBoolean(boolean valueToCompose) {
        if (valueToCompose) {
            return "1";
        }
        return "0";
    }

    /**
     * Converts an ArrayList of {@link DataRecord}s to a new ArrayList of composed {@link DataRecord}s.
     *
     * @param dataRecords ArrayList which should be converted
     * @return ArrayList of composed {@link DataRecord}s
     */
    public static ArrayList<String> getComposedDataRecordArray(ArrayList<DataRecord> dataRecords) {
        ArrayList<String> composedDataRecordArray = new ArrayList<>();
        for (DataRecord dataRecord : dataRecords) {
            composedDataRecordArray.add(composeDataRecord(dataRecord));
        }
        return composedDataRecordArray;
    }
}
