package de.unistuttgart.ipvs.rdbsync.model;


/**
 * Represents a record which is saved in the local database system. It can transferred between the server and the
 * mobile device using appropriate methods from {@link de.unistuttgart.ipvs.rdbsync.util.Composer},
 * {@link de.unistuttgart.ipvs.rdbsync.util.Parser} and
 * {@link de.unistuttgart.ipvs.rdbsync.SyncProcessor}.
 */
public class DataRecord {

    private int sid;
    private int cid;
    private int version;
    private int timestamp;
    private String content;

    public DataRecord(int sid, int cid, int version, int timestamp, String content) {
        this.sid = sid;
        this.cid = cid;
        this.version = version;
        this.timestamp = timestamp;
        this.content = content;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
