package de.unistuttgart.ipvs.rdbsync.model;


/**
 * Contains different constants which may affect the whole application.
 */
public class Constants {

    public static final String LOGGER_ID = "rdbync.logger";

}
