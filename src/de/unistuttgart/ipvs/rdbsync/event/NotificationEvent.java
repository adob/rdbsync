package de.unistuttgart.ipvs.rdbsync.event;


/**
 * Used as a superclass to enable notifications of {@link de.unistuttgart.ipvs.rdbsync.SyncProcessor} instances.
 * An <code>identifier</code> is used to distinguish between senders and recipients.
 */
public class NotificationEvent {

    private String identifier;

    public NotificationEvent(String identifier) {
        this.identifier = identifier;
    }

    public String getIdentifier() {
        return identifier;
    }
}
