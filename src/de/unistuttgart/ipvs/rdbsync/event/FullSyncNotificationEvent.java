package de.unistuttgart.ipvs.rdbsync.event;


/**
 * Thrown when {@link de.unistuttgart.ipvs.rdbsync.SyncProcessor} instances should be notified to perform a full
 * synchronization with the client.
 */
public class FullSyncNotificationEvent extends NotificationEvent {

    public FullSyncNotificationEvent(String identifier) {
        super(identifier);
    }

}
