package de.unistuttgart.ipvs.rdbsync.event;

import de.unistuttgart.ipvs.rdbsync.model.DataRecord;


/**
 * Thrown when {@link de.unistuttgart.ipvs.rdbsync.SyncProcessor} instances should be notified about an deletion
 * of a {@link DataRecord}.
 */
public class DataRecordDeletedNotificationEvent extends NotificationEvent {

    private DataRecord dataRecord;


    public DataRecordDeletedNotificationEvent(String identifier, DataRecord dataRecord) {
        super(identifier);
        this.dataRecord = dataRecord;
    }

    public DataRecord getDataRecord() {
        return dataRecord;
    }
}
