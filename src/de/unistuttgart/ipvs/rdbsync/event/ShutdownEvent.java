package de.unistuttgart.ipvs.rdbsync.event;


/**
 * Thrown when shutdown is in progress to terminate a {@link de.unistuttgart.ipvs.rdbsync.SyncServerThread} in a
 * graceful way.
 */
public class ShutdownEvent {
    /* no contents */
}
